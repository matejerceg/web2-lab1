var router = require('express').Router();
const { requiresAuth } = require('express-openid-connect');
const { body, validationResult } = require('express-validator');
const db = require('../database');
var server = require('../server.js');

var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();

let listaGrupa = ["a","b","c","d","e","f","g","h"]

router.get('/', function (req, res, next) {
  try{
    res.render('index', {
      title: 'Auth0 Webapp sample Nodejs',
      isAuthenticated: req.oidc.isAuthenticated(),
      username: req.oidc.user.nickname
    });
  } catch (e) {
    res.render('index', {
      title: 'UEFA Liga prvaka',
      isAuthenticated: req.oidc.isAuthenticated()
    });
  }
});


router.get('/profile', requiresAuth(), function (req, res, next) {
  res.render('profile', {
    userProfile: JSON.stringify(req.oidc.user, null, 2),
    title: 'Profile page'
  });
});

router.get('/raspored', async function (req, res, next){
  let utakmice = (await db.query('select * from utakmica order by utakmicaid')).rows
  let lista = [];

  requiresAuth();

  let user = ''
  for(let i = 0; i < utakmice.length ; i++){
    let utakmica = new Map();
    let tim1 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim1id])).rows
    let tim2 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim2id])).rows
    let rezultat = utakmice[i].rezultat
    let vrijeme = utakmice[i].vrijeme

    utakmica.set("tim1", tim1[0].timime)
    utakmica.set("rezultat", rezultat)
    utakmica.set("tim2", tim2[0].timime)
    utakmica.set("vrijeme", vrijeme)

    lista.push(utakmica);
  }
  try{
    res.render('raspored', {
      lista : lista,
      url : req.url,
      username: req.oidc.user.nickname
    });
  } catch (e) {
    res.render('raspored', {
      lista : lista,
      url : req.url
    });
  }
});

router.get('/raspored/admin', requiresAuth(), async function (req, res, next){
  let utakmice = (await db.query('select * from utakmica order by utakmicaid')).rows
  let lista = [];

  let loggedInUser = (await db.query('select * from users where email = $1', [req.oidc.user.email])).rows
  let user = loggedInUser[0].username;

  for(let i = 0; i < utakmice.length ; i++){
    let utakmica = new Map();
    let utakmicaid = utakmice[i].utakmicaid
    let tim1 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim1id])).rows
    let tim2 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim2id])).rows
    let rezultat = utakmice[i].rezultat
    let vrijeme = utakmice[i].vrijeme

    utakmica.set("tim1id", utakmice[i].tim1id)
    utakmica.set("tim2id", utakmice[i].tim2id)
    utakmica.set("tim1", tim1[0].timime)
    utakmica.set("rezultat", rezultat)
    utakmica.set("tim2", tim2[0].timime)
    utakmica.set("vrijeme", vrijeme)
    utakmica.set("utakmicaid", utakmicaid)

    lista.push(utakmica);
  }
  if(user != 'admin'){
    res.render('neovlasten', {
      lista : lista,
      url : '.',
      username: req.oidc.user.nickname
    });
  } else {
    res.render('rasporedAdmin', {
      lista : lista,
      user : user,
      url : req.url,
      username: req.oidc.user.nickname
    });
  }
});

router.post('/raspored/admin/add', requiresAuth(), [
  body('utakmicaid'),
  body('golTim1'),
  body('golTim2'),
  body('tim1id'),
  body('tim2id')
  ],
  async function(req, res, next){
    let rezultat = req.body.golTim1 + ":" + req.body.golTim2
    console.log(rezultat);
    let prethodnoStanje = (await db.query('SELECT * FROM utakmica WHERE utakmicaid = $1', 
                [
                  req.body.utakmicaid
                ]
    )).rows;
    let r = prethodnoStanje[0].rezultat;
    let g1 = r.substring(0,r.indexOf(":"));
    let g2 = r.substring(r.indexOf(":")+1);
    let prethStatus = 3;
    if(g1!=="-" && g2!=="-"){
      if(g1>g2){
        prethStatus = 1;
      } else if(g2>g1){
        prethStatus = 2;
      } else {
        prethStatus = 0;
      }
    }

    console.log(prethodnoStanje[0])
    await db.query('UPDATE utakmica SET rezultat = $1 WHERE utakmicaid = $2',
                [
                    rezultat,
                    req.body.utakmicaid
                ],
    );
    let tim1 = (await db.query('SELECT * from tim where timId = $1',
                [
                  req.body.tim1id
                ]
    )).rows;
    let tim2 = (await db.query('SELECT * from tim where timId = $1',
                [
                  req.body.tim2id
                ]
    )).rows;

    console.log(tim1)
    console.log(tim2)

    let bodoviTim1 = null;
    let bodoviTim2 = null;

    if(req.body.golTim1 > req.body.golTim2){
      //ako je prije izmjene bilo tim1>tim2
      if(prethStatus == 1){
        bodoviTim1  = tim1[0].bodovi;
        bodoviTim2  = tim2[0].bodovi;
      } 
      //ako je prije izmjene bio tim2>tim1
      else if(prethStatus == 2){
        bodoviTim1  = tim1[0].bodovi + 3;
        bodoviTim2  = tim2[0].bodovi - 3;

      }
      //ako je prije izmjene bilo tim1=tim2
      else if(prethStatus == 0){
        bodoviTim1  = tim1[0].bodovi - 1 + 3;
        bodoviTim2  = tim2[0].bodovi -1;
      }
      //ako prije izmjene nije bilo rezultata, tj. ako se dodaje novi rezultat
      else if(prethStatus == 3){
        bodoviTim1  = tim1[0].bodovi +3;
        bodoviTim2  = tim2[0].bodovi;
      }

    } else if(req.body.golTim1 < req.body.golTim2) {
      //ako je prije izmjene bilo tim1>tim2
      if(prethStatus == 1){
        console.log(tim1[0].bodovi)
        console.log(tim2[0].bodovi)
        bodoviTim1  = tim1[0].bodovi - 3;
        bodoviTim2  = tim2[0].bodovi + 3;
        console.log(bodoviTim1);
        console.log(bodoviTim2);
      } 
      //ako je prije izmjene bio tim2>tim1
      else if(prethStatus == 2){
        bodoviTim1  = tim1[0].bodovi;
        bodoviTim2  = tim2[0].bodovi;

      }
      //ako je prije izmjene bilo tim1=tim2
      else if(prethStatus == 0){

        bodoviTim1  = tim1[0].bodovi - 1;
        bodoviTim2  = tim2[0].bodovi - 1 + 3;
      }
      //ako prije izmjene nije bilo rezultata, tj. ako se dodaje novi rezultat
      else if(prethStatus == 3){
        bodoviTim1  = tim1[0].bodovi;
        bodoviTim2  = tim2[0].bodovi + 3;
      }

    } else if(req.body.golTim1 = req.body.golTim2){
      //ako je prije izmjene bilo tim1>tim2
      if(prethStatus == 1){
        bodoviTim1  = tim1[0].bodovi - 3 + 1;
        bodoviTim2  = tim2[0].bodovi + 1;
        //golRazlikaTim1 = tim1[0].golrazlika;
        //golRazlikaTim2 = tim2[0].golrazlika;
      } 
      //ako je prije izmjene bio tim2>tim1
      else if(prethStatus == 2){
        bodoviTim1  = tim1[0].bodovi +1;
        bodoviTim2  = tim2[0].bodovi - 3 +1;

      }
      //ako je prije izmjene bilo tim1=tim2
      else if(prethStatus == 0){
        bodoviTim1  = tim1[0].bodovi;
        bodoviTim2  = tim2[0].bodovi;
      }
      //ako prije izmjene nije bilo rezultata, tj. ako se dodaje novi rezultat
      else if(prethStatus == 3){
        bodoviTim1  = tim1[0].bodovi + 1;
        bodoviTim2  = tim2[0].bodovi + 1;
      }
    }
    await db.query('UPDATE tim SET bodovi = $1 WHERE timId = $2',
                [
                    bodoviTim1,
                    req.body.tim1id
                ],
      );
      await db.query('UPDATE tim SET bodovi = $1 WHERE timId = $2',
                [
                    bodoviTim2,
                    req.body.tim2id
                ],
      );
    next();
}, async function (req, res, next){
  let redirectUrl = req.url;
  res.redirect(redirectUrl.substring(0,redirectUrl.lastIndexOf('/')));
});

router.get('/trenutnoStanje', async function (req, res, next) {
  let timovi = (await db.query('select * from tim order by bodovi desc, golRazlika desc')).rows
  try {
    res.render('trenutnoStanje', {
      timovi : timovi,
      username: req.oidc.user.nickname
    });
  } catch (error) {
    res.render('trenutnoStanje', {
      timovi : timovi
    });
  }
});

router.get('/pregledPoKolima/:grupa', requiresAuth(), async function (req, res, next) {
  let grupa = listaGrupa.indexOf(req.params.grupa)+1;
  console.log(grupa)
  const loggedInUser = req.oidc.user.email;
  console.log(req.oidc);
  console.log(loggedInUser)
  
  let users = (await db.query('select * from users')).rows
  res.render('prikazGrupe',{
    grupa : grupa,
    grupaNaziv : listaGrupa[grupa-1],
    username: req.oidc.user.nickname})
});

router.get('/pregledPoKolima/:grupa/:kolo', requiresAuth(), async function (req, res, next) {
  let utakmica1 = new Map();
  let utakmica2 = new Map();
  let lista = [];

  let loggedInUser = (await db.query('select * from users where email = $1', [req.oidc.user.email])).rows
  let user = loggedInUser[0].username;
  
  let utakmice = (await db.query('select * from utakmica where idKolo = $1', [req.params.kolo])).rows

  for(let i = 0; i < utakmice.length ; i++){
    let utakmica = new Map();
    let tim1 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim1id])).rows
    let tim2 = (await db.query('select * from tim where timId = $1', [utakmice[i].tim2id])).rows
    let rezultat = utakmice[i].rezultat
    let vrijeme = utakmice[i].vrijeme

    utakmica.set("tim1", tim1[0].timime)
    utakmica.set("rezultat", rezultat)
    utakmica.set("tim2", tim2[0].timime)
    utakmica.set("vrijeme", vrijeme)

    lista.push(utakmica);
    console.log(lista)
  }

  let komentari = (await db.query('select * from komentar where koloId = $1 order by komentarid', [req.params.kolo])).rows
  console.log(komentari)

  res.render('prikazKola', {
    /*tim1 : utakmica1.get("tim1"),
    tim2 : utakmica1.get("tim2"),
    rezultat : utakmica1.get("rezultat"),
    vrijeme : vrijeme1,*/
    url: req.url,
    komentari: komentari,
    user : user,
    lista : lista,
    username: req.oidc.user.nickname})
});

router.post('/pregledPoKolima/:grupa/:kolo', requiresAuth(), [
  body('username'),
  body('sadrzaj')
      .trim()
      .isLength({min: 1, max: 100})
  ],
  async function(req, res, next){    
    console.log(req.body.username);
    console.log(req.body.sadrzaj);

    let dateTime = new Date();
    let godina = dateTime.getFullYear();
    let mjesec = ("0" + (dateTime.getMonth() + 1)).slice(-2);
    let dan = ("0" + dateTime.getDate()).slice(-2);
    let sat = dateTime.getHours();
    let min = ("0" + dateTime.getMinutes()).slice(-2);

    let datum = dan + "." + mjesec + "." + godina + ". " + sat + ":" + min;
    await db.query('insert into komentar (koloId, username, vrijeme, sadrzaj) values ($1, $2, $3, $4)',
                [
                    req.params.kolo,
                    req.body.username,
                    datum,
                    req.body.sadrzaj,
                ],
            );
    next();
}, function (req, res, next) {
  res.redirect(req.url);
  

});

router.post('/pregledPoKolima/:grupa/:kolo/delete', requiresAuth(), [
  body('komentarid'),
  ],
  async function (req, res, next) {
    console.log(req.body.komentarid)
    await db.query('DELETE FROM komentar WHERE komentarid = $1',
                [
                    req.body.komentarid,
                ],
            );
    next();
}, function (req, res, next) {
  let redirectUrl = req.url;
  res.redirect(redirectUrl.substring(0,redirectUrl.lastIndexOf('/')));
});

router.post('/pregledPoKolima/:grupa/:kolo/edit', requiresAuth(), [
  body('komentarid'),
  ],
  async function (req, res, next) {
    console.log(req.body.komentarid)
    let datum = (await db.query('select * from komentar WHERE komentarid = $1',
                [
                    req.body.komentarid
                ],
            )).rows;
    await db.query('UPDATE komentar SET sadrzaj = $1, vrijeme = $2 WHERE komentarid = $3',
                [
                    req.body.sadrzaj,
                    datum[0].vrijeme,
                    req.body.komentarid
                ],
            );
    next();
}, function (req, res, next) {
  let redirectUrl = req.url;
  res.redirect(redirectUrl.substring(0,redirectUrl.lastIndexOf('/')));
});

router.get('/neovlasten', function (req, res, next){
  try {
    res.render('neovlasten', {
      username: req.oidc.user.nickname
    });
  } catch (error) {
    res.render('neovlasten');
  }
})

module.exports = router;
