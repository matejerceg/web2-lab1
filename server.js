const dotenv = require('dotenv');
const express = require('express');
const http = require('http');
const logger = require('morgan');
const path = require('path');
const router = require('./routes/index');
const { auth } = require('express-openid-connect');
const db = require('./database');

var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();

dotenv.load();


const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// for parsing application/json
app.use(bodyParser.json()); 

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(express.urlencoded({extended: true}));
//form-urlencoded

// for parsing multipart/form-data
app.use(upload.array()); 

app.use(logger('dev'));
app.use(express.static(path.join(__dirname, '/static')));
app.use(express.json());

/*const config = {
  authRequired: false,
  auth0Logout: true
};*/

//

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;


//const port = process.env.PORT || 3000;
if (externalUrl) {
  console.log("----------------");
  console.log(externalUrl)
  const hostname = '127.0.0.1';
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
} else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}

const config = {
  authRequired: false,
  auth0Logout: true,
  baseURL: externalUrl || `https://localhost:${port}`
};
/*if (!config.baseURL && !process.env.BASE_URL && process.env.PORT && process.env.NODE_ENV !== 'production') {
  config.baseURL = `http://localhost:${port}`;
} else{
  console.log("------baseUrl")
}*/

//

app.use(auth(config));

// Middleware to make the `user` object available for all views
app.use(function (req, res, next) {
  res.locals.user = req.oidc.user;
  next();
});

app.use('/', router);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Error handlers
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: process.env.NODE_ENV !== 'production' ? err : {}
  });
});

async function checkRole(email){
  let role = (await db.query('select * from tim')).rows;
  console.log(role);
}

/*http.createServer(app)
  .listen(port, () => {
    console.log(`Listening on ${config.baseURL}`);
  });*/

module.exports = {
  checkRole : checkRole
}
