const {Pool} = require('pg');

const pool = new Pool({
    user: /*'postgres'*/ 'web2_lab1_45oj_user',
    host: /*'localhost'*/ 'dpg-cdeje2kgqg4d3ggounug-a',
    database: /*'web2-lab1'*/ 'web2_lab1_45oj',
    password: /*'bazepodataka'*/ 'OUILDiPJ0A5lvCWToj0M23k4vqbKB8kp',
    port: 5432
    //ssl: true
    //rejectUnauthorized: false
});

const sql_create_users = `
    DROP TABLE IF EXISTS users;
    CREATE TABLE users (
    userId int PRIMARY KEY,
    email text NOT NULL UNIQUE,
    username text NOT NULL UNIQUE,
    role int NOT NULL
)`;

const sql_create_grupa = `
    DROP TABLE IF EXISTS grupa;
    CREATE TABLE grupa(
    grupaId int PRIMARY KEY,
    grupaIme text NOT NULL UNIQUE
)`;

const sql_create_kolo = `
    DROP TABLE IF EXISTS kolo;
    CREATE TABLE kolo(
    koloId int PRIMARY KEY,
    grupaId int NOT NULL,
    brojKola text NOT NULL
)`;

const sql_create_utakmica = `
    DROP TABLE IF EXISTS utakmica;
    CREATE TABLE utakmica(
    utakmicaId int PRIMARY KEY,
    tim1id int,
    tim2id int,
    idKolo int,
    rezultat text,
    vrijeme text
)`;

const sql_create_tim = `DROP TABLE IF EXISTS tim;
    CREATE TABLE tim (
    timId int PRIMARY KEY,
    timIme text NOT NULL UNIQUE,
    grupaId int NOT NULL,
    ukupno int NOT NULL,
	pobjeda int NOT NULL,
	nerijeseno int NOT NULL,
    poraz int NOT NULL,
    golRazlika int,
    bodovi int
)`;

const sql_create_komentar = `
    DROP TABLE IF EXISTS komentar;
    CREATE TABLE komentar (
    komentarId int GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    koloId int NOT NULL,
    username text NOT NULL,
    vrijeme text NOT NULL,
    sadrzaj text NOT NULL
)`;

const sql_insert_komentar= `
    INSERT INTO komentar(koloId, username, vrijeme, sadrzaj)
    VALUES
    (1, 'ana', '21.9.2022. 15:35', 'bla bla bla komentar bla bla'),
    (1, 'pero', '21.9.2022. 16:05', 'aaaaa komentar2')
`;

const sql_insert_utakmica = `
    INSERT INTO utakmica(utakmicaid, tim1id, tim2id, idKolo, rezultat, vrijeme)
    VALUES
    (1,3, 4, 1, '4:0', '07.09.22.	18:45'),
    (2,1, 2, 1, '4:1', '07.09.22.	21:00'),
    (3,2, 3, 2, '2:1', '13.09.22.	21:00'),
    (4,4, 1, 2, '0:3', '14.09.22.	21:00'),
    (5,2, 4, 3, '2:0', '04.10.22.	21:00'),
    (6,3, 1, 3, '1:6', '04.10.22.	21:00'),
    (7,4, 2, 4, '1:7', '12.10.22.	21:00'),
    (8,1, 3, 4, '4:2', '12.10.22.	18:45'),
    (9,1, 4, 5, '-:-', '26.10.22.	21:00'),
    (10,3, 2, 5, '-:-', '26.10.22.	21:00'),
    (11,4, 3, 6, '-:-', '01.11.22.	21:00'),
    (12,2, 1, 6, '-:-', '01.11.22.	21:00'),

    (13,7, 6, 7, '2:1', '07.09.22.	21:00'),
    (14,5, 8, 7, '1:0', '07.09.22.	21:00'),
    (15,8, 7, 8, '2:0', '13.09.22.	21:00'),
    (16,6, 5, 8, '0:4', '13.09.22.	21:00'),
    (17,6, 8, 9, '2:0', '04.10.22.	21:00'),
    (18,5, 7, 9, '2:0', '04.10.22.	21:00'),
    (19,8, 6, 10, '0:3', '12.10.22. 21:00'),
    (20,7, 5, 10, '0:0', '12.10.22. 18:45'),
    (21,7, 8, 11, '-:-', '26.10.22. 21:00'),
    (22,5, 6, 11, '-:-', '26.10.22. 18:45'),
    (23,8, 5, 12, '-:-', '01.11.22. 18:45'),
    (24,6, 7, 12, '-:-', '01.11.22. 18:45'),

    (25,11, 12, 13, '5:1', '07.09.22.	21:00'),
    (26,10, 9, 13, '0:2', '07.09.22.	21:00'),
    (27,9, 11, 14, '2:0', '13.09.22.	21:00'),
    (28,12, 10, 14, '0:2', '13.09.22.	18:45'),
    (29,9, 12, 15, '5:0', '04.10.22. 18:45'),
    (30,10, 11, 15, '1:0', '04.10.22. 21:00'),
    (31,11, 10, 16, '3:3', '12.10.22. 21:00'),
    (32,12, 9, 16, '2:4', '12.10.22. 21:00'),
    (33,10, 12, 17, '-:-', '26.10.22. 18:45'),
    (34,11, 9, 17, '-:-', '26.10.22. 21:00'),
    (35,9, 10, 18, '-:-', '01.11.22. 21:00'),
    (36,12, 11, 18, '-:-', '01.11.22. 21:00'),

    (37,16, 15, 19, '0:3', '07.09.22.	18:45'),
    (38,13, 14, 19, '2:0', '07.09.22.	21:00'),
    (39,14, 16, 20, '0:1', '13.09.22.	21:00'),
    (40,15, 13, 20, '2:0', '13.09.22.	18:45'),
    (41,14, 15, 21, '4:1', '04.10.22. 18:45'),
    (42,16, 13, 21, '0:0', '04.10.22. 21:00	'),
    (43,15, 14, 22, '0:2', '12.10.22. 21:00	'),
    (44,13, 16, 22, '3:2', '12.10.22. 21:00	'),
    (45,13, 15, 23, '-:-', '26.10.22. 21:00'),
    (46,16, 14, 23, '-:-', '26.10.22. 21:00'),
    (47,14, 13, 24, '-:-', '01.11.22.	21:00	'),
    (48,15, 16, 24, '-:-', '01.11.22.	21:00'),

    (49,18, 20, 25, '1:1', '06.09.22.	21:00'),
    (50,19, 17, 25, '1:0', '06.09.22.	18:45'),
    (51,20, 19, 26, '3:1', '14.09.22.	18:45'),
    (52,17, 18, 26, '1:1', '14.09.22.	21:00'),
    (53,17, 20, 27, '3:0', '05.10.22.	21:00	'),
    (54,18, 19, 27, '1:0', '05.10.22.	18:45	'),
    (55,20, 17, 28, '0:2', '11.10.22.	21:00'),
    (56,19, 18, 28, '1:1', '11.10.22.	21:00	'),
    (57,18, 17, 29, '-:-', '25.10.22.	18:45	'),
    (58,19, 20, 29, '-:-', '25.10.22.	21:00	'),
    (59,17, 19, 30, '-:-', '02.11.22.	21:00'),
    (60,20, 18, 30, '-:-', '02.11.22.	21:00	'),

    (61,24, 21, 31, '0:3', '06.09.22.	21:00'),
    (62,22, 23, 31, '1:4', '06.09.22.	21:00'),
    (63,21, 22, 32, '2:0', '14.09.22.	21:00'),
    (64,23, 24, 32, '1:1', '14.09.22.	18:45'),
    (65,22, 24, 33, '3:1', '05.10.22.	18:45'),
    (66,21, 23, 33, '2:1', '05.10.22.	21:00	'),
    (67,23, 21, 34, '1:1', '11.10.22.	21:00	'),
    (68,24, 22, 34, '0:2', '11.10.22.	21:00	'),
    (69,24, 23, 35, '-:-', '25.10.22.	21:00	'),
    (70,22, 21, 35, '-:-', '25.10.22.	21:00	'),
    (71,21, 24, 36, '-:-', '02.11.22.	18:45	'),
    (72,23, 22, 36, '-:-', '02.11.22.	18:45	'),

    (73,27, 25, 37, '0:4', '06.09.22.	21:00'),
    (74,26, 28, 37, '3:0', '06.09.22.	18:45'),
    (75,25, 26, 38, '2:1', '14.09.22.	21:00'),
    (76,28, 27, 38, '0:0', '14.09.22.	21:00'),
    (77,27, 26, 39, '1:4', '05.10.22.	21:00	'),
    (78,25, 28, 39, '5:0', '05.10.22.	21:00	'),
    (79,28, 25, 40, '0:0', '11.10.22.	18:45	'),
    (80,26, 27, 40, '1:1', '11.10.22.	21:00	'),
    (81,26, 25, 41, '-:-', '25.10.22.	21:00	'),
    (82,27, 28, 41, '-:-', '25.10.22.	18:45	'),
    (83,28, 26, 42, '-:-', '02.11.22.	21:00'),
    (84,25, 27, 42, '-:-', '02.11.22.	21:00'),

    (85,29, 31, 43, '2:1', '06.09.22.	21:00'),
    (86,30, 32, 43, '2:0', '06.09.22.	21:00'),
    (87,31, 30, 44, '1:2', '14.09.22.	21:00'),
    (88,32, 29, 44, '1:3', '14.09.22.	21:00'),
    (89,31, 32, 45, '3:1', '05.10.22.	21:00'),
    (90,30, 29, 45, '1:1', '05.10.22.	21:00'),
    (91,32, 31, 46, '2:0', '11.10.22.	18:45'),
    (92,29, 30, 46, '1:1', '11.10.22.	21:00	'),
    (93,30, 31, 47, '-:-', '25.10.22.	21:00	'),
    (94,29, 32, 47, '-:-', '25.10.22.	21:00	'),
    (95,31, 29, 48, '-:-', '02.11.22.	21:00	'),
    (96,32, 30, 48, '-:-', '02.11.22.	21:00	');
    `

const sql_insert_grupa = `INSERT INTO grupa(
    grupaId, grupaIme)
    VALUES 
    (1, 'Grupa A'),
    (2, 'Grupa B'),
    (3, 'Grupa C'),
    (4, 'Grupa D'),
    (5, 'Grupa E'),
    (6, 'Grupa F'),
    (7, 'Grupa G'),
    (8, 'Grupa H');
`;

const sql_insert_kolo = ``

const sql_insert_tim = `
    INSERT INTO tim(timId, timIme, grupaId, ukupno, pobjeda, nerijeseno, poraz, golRazlika, bodovi)
    VALUES 
    (1, 'Napoli', 1, 4,4,0,0,13,12),
    (2, 'Liverpool', 1, 4,3,0,1,6,9),
    (3, 'Ajax', 1, 4,1,0,3,-4,3),
    (4, 'FC Rangers', 1, 4,0,0,4,-15,0),
    
	(5, 'Club Brugge', 2, 4,3,1,0,7,10),
    (6, 'Porto', 2, 4,2,0,2,0,6),
    (7, 'Atletico Madrid', 2, 4,1,1,2,-3,4),
    (8, 'Bayer Leverkusen', 2, 4,1,0,3,-4,3),
    
	(9, 'Bayern Munich', 3, 4,4,0,0,11,12),
    (10, 'Internazionale', 3, 4,2,1,1,1,7),
    (11, 'Barcelona', 3, 4,1,1,2,1,4),
    (12, 'Viktoria Plzen', 3, 4,0,0,4,-13,0),
    
	(13, 'Tottenham', 4, 4,2,1,1,1,7),
    (14, 'Olympique Marseille', 4, 4,2,0,2,2,6),
    (15, 'Sporting Lisabon', 4, 4,2,0,2,0,6),
    (16, 'Eintracht Frankfurt', 4, 4,1,1,2,-3,4),
    
	(17, 'Chelsea', 5, 4,2,1,1,4,7),
    (18, 'RB Salzburg', 5, 4,1,3,0,1,6),
    (19, 'GNK Dinamo Zagreb', 5, 4,1,1,2,-2,4),
    (20, 'Milan', 5, 4,1,1,2,-3,4),
    
	(21, 'Real Madrid', 6, 4,3,1,0,6,10),
    (22, 'RB Leipzig', 6, 4,2,0,2,-1,6),
    (23, 'Shakhtar Donetsk', 6, 4,1,2,1,2,5),
    (24, 'Celtic', 6, 4,0,1,3,-7,1),
    
	(25, 'Manchester City', 7, 4,3,1,0,10,10),
    (26, 'Borussia Dortmund', 7, 4,2,1,1,5,7),
    (27, 'Sevilla', 7, 4,0,2,2,-7,2),
    (28, 'FC Copenhagen', 7, 4,0,2,2,-8,2),
    
	(29, 'PSG', 8, 4,2,2,0,3,8),
    (30, 'Benfica', 8, 4,2,2,0,3,8),
    (31, 'Juventus', 8, 4,1,0,3,-2,3),
    (32, 'Maccabi Haifa', 8, 4,1,0,3,-4,3);
`;

const sql_insert_users = `
    INSERT INTO users(
    userId, email, username, role)
    VALUES 
    (1, '59d5s.admin@inbox.testmail.app', 'admin', 1),
    (2, '59d5s.ana@inbox.testmail.app', 'ana', 2),
    (3, '59d5s.pero@inbox.testmail.app', 'pero', 2),
    (5, '59d5s.iva@inbox.testmail.app', 'iva', 2),
	(6, '59d5s.luka@inbox.testmail.app', 'luka', 2),
	(7, '59d5s.maja@inbox.testmail.app', 'maja', 2),
	(8, '59d5s.marko@inbox.testmail.app', 'marko', 2)
`;



pool.query(sql_create_grupa, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'grupa' table");
    pool.query(sql_insert_grupa, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});

pool.query(sql_create_tim, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'tim' table");
    pool.query(sql_insert_tim, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});

pool.query(sql_create_users, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'users' table");
    pool.query(sql_insert_users, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});

pool.query(sql_create_kolo, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'kolo' table");
    pool.query(sql_insert_kolo, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});

pool.query(sql_create_utakmica, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'utakmica' table");
    pool.query(sql_insert_utakmica, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});

pool.query(sql_create_komentar, [], (err, result) => {
    if (err) {
        return console.error(err.message);
    }
    console.log("Successful creation of the 'komentar' table");
    pool.query(sql_insert_komentar, [], (err, result) => {
        if (err) {
            return console.error(err.message);
        }
    });
});